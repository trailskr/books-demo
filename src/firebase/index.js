import firebase from 'firebase/app'

import 'firebase/firestore'
import 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyCBUMPGobH3zJWXO0B9W3RMh60KoeumN-k',
  authDomain: 'books-b4671.firebaseapp.com',
  databaseURL: 'https://books-b4671.firebaseio.com',
  projectId: 'books-b4671',
  storageBucket: 'books-b4671.appspot.com',
  messagingSenderId: '84753983041',
  appId: '1:84753983041:web:7982d7315122380330c8c0'
}

firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore()
export const storage = firebase.storage()

function warn (fieldLabel, expectedType, valueType, value) {
  console.error(`\
trying to set field ${fieldLabel} of type ${expectedType}\
with incorrect value: ${JSON.stringify(value)} of type ${valueType}`)
  return false
}

function checkFieldValue (fieldLabel, expectedType, value) {
  const receivedType = typeof value
  switch (expectedType) {
    case 'time':
      if (receivedType !== 'string' || !value.match(/^[0-2]\d:[0-5]\d$/)) {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    case 'date':
      if (receivedType !== 'string' || !value.match(/^\d{4}-[01]\d-[0-3]\d$/)) {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    case 'dateTime':
      if (
        receivedType !== 'string' ||
        !value.match(/^\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z?)$/)
      ) {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    case 'boolean':
      if (receivedType !== 'boolean') {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    case 'string':
      if (receivedType !== 'string') {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    case 'integer':
      if (receivedType !== 'number' && (~~value !== value)) {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    case 'number':
      if (receivedType !== 'number') {
        return warn(fieldLabel, expectedType, receivedType, value)
      }
      break
    default:
      throw new Error(`unsupported type ${expectedType}`)
  }
  return true
}

/**
 * Check validity of object fields and return new object with only proper fields
 * @param object {Object} Incoming object
 * @param rules {Object<string, {required: boolean, type: string}>}
 *   Every key has rule for object field with the same key
 * return {Object}
 */
export function trimObject (object, rules) {
  const result = {}

  Object.keys(rules).forEach(key => {
    const fieldRules = rules[key]
    const value = object[key]
    if (value == null) {
      result[key] = null
      if (fieldRules.required) {
        warn(key, fieldRules.type, typeof value, value)
      }
    } else {
      if (checkFieldValue(key, fieldRules.type, value)) {
        result[key] = value
      } else {
        result[key] = null
      }
    }
  })

  return result
}

/**
 * create new object with fields initialized
 * @param rules {Object<string, {required: boolean, type: string}>}
 *   Every key has rule for object field with the same key
 * return {Object}
 */
export function initObject (rules) {
  const result = {}

  Object.keys(rules).forEach(key => {
    result[key] = null
  })

  return result
}
